package aises

import (
    "context"
    "crypto/rand"
    "encoding/hex"
    "fmt"
    "net/http"
    "net/textproto"
    "net/url"
    "time"
)

var ctx context.Context

type key uint64

const (
    sessionCtx key = iota
)

type Manager struct {
    provider        Provider
    config          *Options
}

type Options struct {
    CookieName              string          `json:"cookieName"`
    EnableSetCookie         bool            `json:"enableSetCookie,omitempty"`
    GCLifeTime              int64           `json:"gcLifeTime"`
    MaxLifeTime             int64           `json:"maxLifeTime"`
    DisableHttpOnly         bool            `json:"disableHttpOnly"`
    Secure                  bool            `json:"secure"`
    CookieLifeTime          int             `json:"cookieLifeTime"`
    ProviderConfig          ProviderConfig  `json:"providerConfig"`
    Domain                  string          `json:"domain"`
    SessionIDLength         int64           `json:"sessionIDLength"`
    EnableSidInHttpHeader   bool            `json:"EnableSidInHttpHeader"`
    SessionNameInHttpHeader string          `json:"SessionNameInHttpHeader"`
    EnableSidInUrlQuery     bool            `json:"EnableSidInUrlQuery"`
}

type ProviderConfig interface {
    ProviderName() string
}

var m *Manager

var providers = make(map[string]Provider)

func Register(name string, provider Provider) {
    if provider == nil {
        panic(Error("Register provider is nil"))
    }
    if _, dup := providers[name]; dup {
        panic(Error("Register called twice for provider " + name))
    }
    providers[name] = provider
}

func NewManager(providerName string, opt *Options) (*Manager, error) {
    provider, ok := providers[providerName]
    if !ok {
        return nil, Error(fmt.Sprintf("Unknown provide %q (forgotten import?)", providerName))
    }
    if opt.MaxLifeTime == 0 {
        opt.MaxLifeTime = opt.GCLifeTime
    }
    if opt.EnableSidInHttpHeader {
        if opt.SessionNameInHttpHeader == "" {
            panic(Error("SessionNameInHttpHeader is empty"))
        }

        strMimeHeader := textproto.CanonicalMIMEHeaderKey(opt.SessionNameInHttpHeader)
        if opt.SessionNameInHttpHeader != strMimeHeader {
            panic(Error(fmt.Sprintf("SessionNameInHttpHeader (%s) has the wrong format, it should be like this : %s", opt.SessionNameInHttpHeader, strMimeHeader)))
        }
    }

    err := provider.SessionInit(opt.MaxLifeTime, opt.ProviderConfig)
    if err != nil {
        return nil, err
    }
    if opt.SessionIDLength == 0 {
        opt.SessionIDLength = 32
    }
    m = &Manager{
        provider,
        opt,
    }
    go m.GC()
    return m, nil
}

func GetManager() *Manager {
    if m == nil {
        panic(Error("Not initialize Manager"))
    }
    return m
}

func (m *Manager) getSid(r *http.Request) (string, error) {
    cookie, err := r.Cookie(m.config.CookieName)
    if err != nil || cookie.Value == "" {
        var sid string
        if m.config.EnableSidInUrlQuery {
            err := r.ParseForm()
            if err != nil {
                return sid, err
            }

            sid = r.FormValue(m.config.CookieName)
        }
        if m.config.EnableSidInHttpHeader && sid == "" {
            sids, isFound := r.Header[m.config.SessionNameInHttpHeader]
            if isFound && len(sids) != 0 {
                sid = sids[0]
            }
        }

        return sid, nil
    }
    return url.QueryUnescape(cookie.Value)
}

func (m *Manager) sessionID() (string, error) {
    b := make([]byte, m.config.SessionIDLength)
    n, err := rand.Read(b)
    if n != len(b) || err != nil {
        return "", Error("Could not successfully read from the system CSPRNG")
    }
    return hex.EncodeToString(b), nil
}

func (m *Manager) isSecure(r *http.Request) bool {
    if !m.config.Secure {
        return false
    }
    if r.URL.Scheme != "" {
        return r.URL.Scheme == "https"
    }
    if r.TLS == nil {
        return false
    }
    return true
}

func (m *Manager) SessionStart(w http.ResponseWriter, r *http.Request) (Session, error) {
    sid, err := m.getSid(r)
    if err != nil {
        Println(err)
        return nil, err
    }
    if sid != "" && m.provider.SessionExist(sid) {
        return m.provider.SessionRead(sid)
    }

    sid, err = m.sessionID()
    if err != nil {
        Println(err)
        return nil, err
    }

    session, err := m.provider.SessionRead(sid)
    if err != nil {
        Println(err)
        return nil, err
    }
    cookie := &http.Cookie{
        Name:       m.config.CookieName,
        Value:      url.QueryEscape(sid),
        Path:       "/",
        HttpOnly:   !m.config.DisableHttpOnly,
        Secure:     m.isSecure(r),
        Domain:     m.config.Domain,
    }
    if m.config.CookieLifeTime > 0 {
        cookie.MaxAge = m.config.CookieLifeTime
        cookie.Expires = time.Now().Add(time.Duration(m.config.CookieLifeTime) * time.Second)
    }
    if m.config.EnableSetCookie {
        http.SetCookie(w, cookie)
    }
    r.AddCookie(cookie)
    if m.config.EnableSidInHttpHeader {
        r.Header.Set(m.config.SessionNameInHttpHeader, sid)
        w.Header().Set(m.config.SessionNameInHttpHeader, sid)
    }
    return session, nil
}

func (m *Manager) SessionDestroy(w http.ResponseWriter, r *http.Request) {
    if m.config.EnableSidInHttpHeader {
        r.Header.Del(m.config.SessionNameInHttpHeader)
        w.Header().Del(m.config.SessionNameInHttpHeader)
    }
    cookie, err := r.Cookie(m.config.CookieName)
    if err != nil || cookie.Value == "" {
        return
    }

    sid, _ := url.QueryUnescape(cookie.Value)
    m.provider.SessionDestroy(sid)
    if m.config.EnableSetCookie {
        expiration := time.Now()
        cookie = &http.Cookie{
            Name:       m.config.CookieName,
            Path:       "/",
            HttpOnly:   !m.config.DisableHttpOnly,
            Expires:    expiration,
            MaxAge:     -1,
        }
        http.SetCookie(w, cookie)
    }
}

func (m *Manager) GetSessionStore(sid string) (Session, error) {
    return m.provider.SessionRead(sid)
}

func (m *Manager) GC() {
    m.provider.SessionGC()
    time.AfterFunc(time.Duration(m.config.GCLifeTime)*time.Second, func() {m.GC()})
}

func (m *Manager) SessionRegenerateID(w http.ResponseWriter, r *http.Request) (session Session) {
    sid, err := m.sessionID()
    if err != nil {
        return nil
    }
    cookie, err := r.Cookie(m.config.CookieName)
    if err != nil || cookie.Value == "" {
        session, _ = m.provider.SessionRead(sid)
        cookie = &http.Cookie{
            Name:       m.config.CookieName,
            Path:       "/",
            HttpOnly:   !m.config.DisableHttpOnly,
            Secure:     m.isSecure(r),
            Domain:     m.config.Domain,
        }
    } else {
        oldSid, _ := url.QueryUnescape(cookie.Value)
        session, _ = m.provider.SessionRegenerate(oldSid, sid)
        cookie.Value = url.QueryEscape(sid)
        cookie.HttpOnly = !m.config.DisableHttpOnly
        cookie.Path = "/"
    }
    if m.config.CookieLifeTime > 0 {
        cookie.MaxAge = m.config.CookieLifeTime
        cookie.Expires = time.Now().Add(time.Duration(m.config.CookieLifeTime) * time.Second)
    }
    if m.config.EnableSetCookie {
        http.SetCookie(w, cookie)
    }
    r.AddCookie(cookie)
    if m.config.EnableSidInHttpHeader {
        r.Header.Set(m.config.SessionNameInHttpHeader, sid)
        w.Header().Set(m.config.SessionNameInHttpHeader, sid)
    }
    return
}

func (m *Manager) GetActiveSession() int {
    return m.provider.SessionAll()
}

func (m *Manager) SetSecure(secure bool) {
    m.config.Secure = secure
}

func (m *Manager) Use(h http.Handler) http.Handler {
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        ses, err := m.SessionStart(w, r)
        if err != nil {
            Println(err)
        }
        h.ServeHTTP(w, SetSession(w, r, ses))
    })
}

func GetSession(w http.ResponseWriter, r *http.Request) Session {
    s, ok := r.Context().Value(sessionCtx).(Session)
    if ok {
        return s
    }
    s, err := GetManager().SessionStart(w, r)
    if err != nil {
        Println(err)
    }
    return s
}

func SetSession(w http.ResponseWriter, r *http.Request, ses Session) *http.Request {
    ses.Release(w)
    ctx := context.WithValue(r.Context(), sessionCtx, ses)
    return r.WithContext(ctx)
}
