package aises

import "net/http"

type Session interface {
    ID() string
    Set(string, interface{}) error
    Get(string) interface{}
    GetByte(string, []byte) []byte
    GetStr(string, string) string
    GetBool(string, bool) bool
    GetInt(string, int) int
    GetInt32(string, int32) int32
    GetInt64(string, int64) int64
    GetFloat32(string, float32) float32
    GetFloat64(string, float64) float64
    Delete(string) error
    Release(http.ResponseWriter)
    Flush() error
    Save(http.ResponseWriter, *http.Request) *http.Request
}