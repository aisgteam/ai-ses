package aises

import (
    "fmt"
    "log"
    "os"
)

func init() {
    l = log.New(os.Stdout, "[SESSION]", 1e9)
}

var l Logger

type Logger interface {
    Print(...interface{})
    Printf(string, ...interface{})
    Println(...interface{})
}

func SetLogger(log Logger) {
    l = log
}

func Print(v ...interface{}) {
    l.Print(v...)
}

func Printf(format string, v ...interface{}) {
    l.Printf(format, v...)
}

func Println(v ...interface{}) {
    l.Println(v...)
}

type Error string

func (e Error) Error() string {
    return fmt.Sprintf("aises: %s", e)
}

//type Log struct {
//    *log.Logger
//}
//
//func NewSessionLog(out io.Writer) *Log {
//    sl := new(Log)
//    sl.Logger = log.New(out, "[SESISON]", 1e9)
//    return sl
//}