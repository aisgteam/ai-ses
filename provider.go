package aises

type Provider interface {
    SessionInit(int64, ProviderConfig) error
    SessionRead(string) (Session, error)
    SessionExist(string) bool
    SessionRegenerate(string, string) (Session, error)
    SessionDestroy(string) error
    SessionAll() int
    SessionGC()
}