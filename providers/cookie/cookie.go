package cookie

import (
    "bitbucket.org/aisgteam/ai-ses"
    "crypto/aes"
    "crypto/cipher"
    "encoding/json"
    "net/http"
    "net/url"
    "sync"
)

var pr *Provider

type options struct {
    SecurityKey  string `json:"securityKey"`
    BlockKey     string `json:"blockKey"`
    SecurityName string `json:"securityName"`
    CookieName   string `json:"cookieName"`
    Secure       bool   `json:"secure"`
    MaxAge       int    `json:"maxAge"`
}

type Provider struct {
    maxLifeTime int64
    config      *options
    block       cipher.Block
}

func NewProvider() *Provider {
    pr = &Provider{}
    return pr
}

func RegisterProvider() {
    aises.Register("cookie", NewProvider())
}

func (p *Provider) SessionInit(maxLifeTime int64, config string) error {
    p.config = &options{}
    err := json.Unmarshal([]byte(config), p.config)
    if err != nil {
        return err
    }
    if p.config.BlockKey == "" {
        p.config.BlockKey = string(aises.GenerateRandomKey(16))
    }
    if p.config.SecurityName == "" {
        p.config.SecurityName = string(aises.GenerateRandomKey(20))
    }
    p.block, err = aes.NewCipher([]byte(p.config.BlockKey))
    if err != nil {
        return nil
    }
    p.maxLifeTime = maxLifeTime
    return nil
}

func (p *Provider) SessionRead(sid string) (aises.Session, error) {
    maps, _ := aises.DecodeCookie(
        p.block,
        p.config.SecurityKey,
        p.config.SecurityName,
        sid,
        p.maxLifeTime,
    )
    if maps == nil {
        maps = make(map[interface{}]interface{})
    }
    s := &Store{
        sid:   sid,
        value: maps,
    }
    return s, nil
}

func (p *Provider) SessionExist(sid string) bool {
    return true
}

func (p *Provider) SessionRegenerate(oldSid, sid string) (aises.Session, error) {
    return nil, nil
}

func (p *Provider) SessionDestroy(sid string) error {
    return nil
}

func (p *Provider) SessionGC() {

}

func (p *Provider) SessionAll() int {
    return 0
}

func (p *Provider) SessionUpdate(sid string) error {
    return nil
}

type Store struct {
    sid   string
    value map[interface{}]interface{}
    lock  sync.RWMutex
}

func (s *Store) Set(key, value interface{}) error {
    s.lock.Lock()
    defer s.lock.Unlock()
    s.value[key] = value
    return nil
}

func (s *Store) Get(key interface{}) interface{} {
    s.lock.RLock()
    defer s.lock.RUnlock()
    if v, ok := s.value[key]; ok {
        return v
    }
    return nil
}

func (s *Store) GetStr(key interface{}, def string) string {
    v := s.Get(key)
    if v != nil {
        return v.(string)
    }
    return def
}

func (s *Store) GetInt(key interface{}, def int) int {
    v := s.Get(key)
    if v != nil {
        return v.(int)
    }
    return def
}

func (s *Store) GetInt64(key interface{}, def int64) int64 {
    v := s.Get(key)
    if v != nil {
        return v.(int64)
    }
    return def
}

func (s *Store) GetBool(key interface{}, def bool) bool {
    v := s.Get(key)
    if v != nil {
        return v.(bool)
    }
    return def
}

func (s *Store) GetFloat(key interface{}, def float64) float64 {
    v := s.Get(key)
    if v != nil {
        return v.(float64)
    }
    return def
}

func (s *Store) Delete(key interface{}) error {
    s.lock.Lock()
    defer s.lock.Unlock()
    delete(s.value, key)
    return nil
}

func (s *Store) SessionID() string {
    return s.sid
}

func (s *Store) SessionRelease(w http.ResponseWriter) {
    encodedCookie, err := aises.EncodeCookie(
        pr.block,
        pr.config.SecurityKey,
        pr.config.SecurityName,
        s.value,
    )
    if err == nil {
        //fmt.Println("error equal nil")
        //fmt.Println(encodedCookie)
        cookie := &http.Cookie{
            Name:     pr.config.CookieName,
            Value:    url.QueryEscape(encodedCookie),
            Path:     "/",
            HttpOnly: true,
            Secure:   pr.config.Secure,
            MaxAge:   pr.config.MaxAge,
        }
        //fmt.Printf("%+v\n", cookie)
        //fmt.Printf("%+v\n", s)
        http.SetCookie(w, cookie)
        //fmt.Printf("%+v\n", w)
    }
}

func (s *Store) Flush() error {
    s.lock.Lock()
    defer s.lock.Unlock()
    s.value = make(map[interface{}]interface{})
    return nil
}

func init() {
    RegisterProvider()
}
